# ISW 4K4 Grupo 1 2021

Repositorio para almacenar todo el contenido relacionado a la materia Ingenieria de Software

## Informacion del Repositorio

Para más información sobre la estructura del Repositorio ir a: 
- [ISW_Estructura_Repositorio_2021](https://gitlab.com/mauriciotello72/isw-4k4-grupo-1-2021/-/blob/master/Informaci%C3%B3n%20General/Gestion%20de%20Repositorio/ISW_EstructuraRepositorio.md)

- [ISW_ItemsDeConfiguracion](https://gitlab.com/mauriciotello72/isw-4k4-grupo-1-2021/-/blob/master/Informaci%C3%B3n%20General/Gestion%20de%20Repositorio/ISW_ItemsDeConfiguracion.md)

- [ISW_DefinicionDeLineaBase](https://gitlab.com/mauriciotello72/isw-4k4-grupo-1-2021/-/blob/master/Informaci%C3%B3n%20General/Gestion%20de%20Repositorio/ISW_DefinicionDeLineaBase.md)

